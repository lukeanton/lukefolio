export type ButtonType = 'button' | 'submit' | 'reset';

export type ButtonVariantType = 'danger' | 'danger--tertiary' | 'dark' | 'primary' | 'secondary' | 'tertiary';

export type IconIdType =
  | 'blog_icon'
  | 'home_icon'
  | 'like_icon'
  | 'message_icon'
  | 'notification_icon'
  | 'place_icon'
  | 'plus_icon'
  | 'preloader_icon'
  | 'preloader_secondary_icon'
  | 'projects_icon '
  | 'settings_icon';

export type SpacingSizeType =
  | '6x-large'
  | '5x-large'
  | '4x-large'
  | '3x-large'
  | '2x-large'
  | 'x-large'
  | 'large'
  | 'small'
  | 'x-small'
  | '2x-small'
  | '3x-small'
  | 'none';

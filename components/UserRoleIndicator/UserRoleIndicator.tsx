import { UserRoleIndicatorProps } from './UserRoleIndicator.types';

const UserRoleIndicator = ({ additionalClassNames, iconId }: UserRoleIndicatorProps) => {
  return (
    <div>
      {additionalClassNames}
      {iconId}
    </div>
  );
};

export { UserRoleIndicator };

import { IAdditionalClassNames } from '../../interfaces';
import { IconIdType } from '../../types';

export interface UserRoleIndicatorProps extends IAdditionalClassNames {
  /**
   * Specify the icon id for the icon you would like to display
   */
  iconId: IconIdType;
}

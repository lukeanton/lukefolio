import { ReactChild } from 'react';

import { SpacingSizeType } from '../../types';

export interface SpacingProps {
  /**
   * The amount spacing beneath the wrapped components
   */
  children?: ReactChild;
  /**
   * The amount spacing beneath the wrapped components
   */
  size?: SpacingSizeType;
}

import React from 'react';

import { SpacingProps } from './Spacing.types';

/**
 * For creating vertical space between components.
 */
const Spacing = ({ children, size }: SpacingProps) => <div className={size ? `h-spacing-${size}` : 'h-spacing'}>{children}</div>;

export { Spacing };

import { IAdditionalClassNames } from 'interfaces';

export interface IClout {
  /**
   * Specify how much clout you have
   */
  amount: string;
  /**
   * Specify what kind of clout you have
   */
  type: string;
}

export interface CloutPanelProps extends IAdditionalClassNames {
  /**
   * Specify what kind of clout you would like to show off and how much.
   */
  clout: IClout[];
}

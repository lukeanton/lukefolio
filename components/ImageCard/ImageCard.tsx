import React from 'react';

import cx from 'classnames';

import { ImageCardProps } from './ImageCard.types';

const ImageCard = ({ additionalClassNames, coverLink, image, isHero = false, subTitle, title }: ImageCardProps) => {
  const ImageCardClassNames = cx('c-page-preview-card', additionalClassNames, {
    'c-page-preview-card--is-hero': isHero,
  });

  return (
    <article className={ImageCardClassNames}>
      <div className="c-page-preview-card__image-container">
        {image ? <img alt={image.altText} className="c-page-preview-card__image" src={image.src} /> : null}
      </div>
      {subTitle ? <span className="c-page-preview-card__sub-title">{subTitle}</span> : null}
      {title ? <span className="c-page-preview-card__title">{title}</span> : null}
      {coverLink}
    </article>
  );
};

export { ImageCard };

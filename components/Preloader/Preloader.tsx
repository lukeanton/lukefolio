import React from 'react';

import cx from 'classnames';

import { Icon } from '../Icon';

import { PreloaderProps } from './Preloader.types';

const Preloader = ({ additionalClassNames, iconId = 'preloader_icon', isLoading = false }: PreloaderProps) => {
  if (!isLoading) {
    return null;
  }

  const preloaderClassNames = cx('c-preloader-overlay', additionalClassNames);

  return (
    <div className={preloaderClassNames} role="status">
      <div className="c-preloader__wrapper">
        <Icon additionalClassNames="c-preloader__spinner" iconId={iconId} />
      </div>
    </div>
  );
};

export { Preloader };

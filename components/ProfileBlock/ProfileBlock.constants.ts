import { IClout } from '../CloutPanel';

const GRAM_CLOUT: IClout[] = [
  { type: 'posts', amount: '5' },
  { type: 'followers', amount: '21m' },
  { type: 'following', amount: '54' },
];

const CLOUT: IClout[] = [
  { type: 'projects', amount: '4' },
  { type: 'blogs', amount: '3' },
  { type: 'job', amount: '1' },
];

export { CLOUT, GRAM_CLOUT };

/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from 'react';

import { useLazyQuery } from '@apollo/client';

import { DesktopProfileBlock } from './DesktopProfileBlock';
import { MobileProfileBlock } from './MobileProfileBlock.tsx';
import { IMyProfile, ProfileBlockProps } from './ProfileBlock.types';

import { Preloader, Spacing } from '../../components';
import { useToast } from '../../hooks/useToast';
import { IProfilePicture } from '../../interfaces';
import { client } from '../../middleware/client';
import { GET_PROFILE_BY_PROFILE_ID } from '../../middleware/profile/getProfileByProfileId';

const ProfileBlock = ({ additionalClassNames, isMobile = false }: ProfileBlockProps) => {
  const { handleToastError } = useToast();

  const [avatarUrl, setAvatarUrl] = useState<IProfilePicture>();
  const [myProfile, setMyProfile] = useState<IMyProfile>();

  const { bio, name } = myProfile ?? {};

  const [getProfileById, { loading: isGetProfileByIdLoading }] = useLazyQuery(GET_PROFILE_BY_PROFILE_ID, {
    client,
    onCompleted: (response) => {
      const { profile } = response;
      const {
        profilePicture: { url },
      } = profile;

      setMyProfile(profile as IMyProfile);
      setAvatarUrl(url as IProfilePicture);

      //TODO: Do something when the block loads or consolidate profile query
    },
    onError: (error) => {
      handleToastError({
        error,
      });
    },
  });

  useEffect(() => {
    // eslint-disable-next-line @typescript-eslint/no-floating-promises
    getProfileById({
      variables: {
        id: process.env.MY_PROFILE_ID,
      },
    });
  }, []);

  const isLoading = isGetProfileByIdLoading;

  return (
    <>
      <Spacing size="2x-large" />
      {isMobile ? (
        <MobileProfileBlock additionalClassNames={additionalClassNames} bio={bio} name={name} profilePicture={avatarUrl} />
      ) : (
        <DesktopProfileBlock additionalClassNames={additionalClassNames} bio={bio} name={name} profilePicture={avatarUrl} />
      )}

      <Preloader isLoading={isLoading} />
    </>
  );
};

export { ProfileBlock };

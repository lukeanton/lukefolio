import { ProfileBlockProps, IMyProfile } from '../ProfileBlock.types';

export interface DesktopProfileBlockProps extends ProfileBlockProps, IMyProfile {}

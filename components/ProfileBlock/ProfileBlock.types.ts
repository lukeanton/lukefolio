import { IAdditionalClassNames, IIsMobile, IProfilePicture } from 'interfaces';

export interface IMyProfile {
  /**
   * Some test prop
   */
  bio?: string;
  /**
   * Some test prop
   */
  handle?: string;
  /**
   * Some test prop
   */
  name?: string;
  /**
   * Some test prop
   */
  profilePicture?: IProfilePicture;
}

export interface ProfileBlockProps extends IAdditionalClassNames, IIsMobile {}

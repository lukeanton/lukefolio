import cx from 'classnames';

import { MobileProfileBlockProps } from './MobileProfileBlock.types';

import { Avatar } from '../../../components';

const MobileProfileBlock = ({ additionalClassNames, profilePicture, bio, name }: MobileProfileBlockProps) => {
  const profileBlockClassNames = cx('c-mobile-profile-block', additionalClassNames);

  return (
    <div className={profileBlockClassNames}>
      <div className="c-mobile-profile-block__avatar-section">
        <Avatar additionalClassNames="c-mobile-profile-block__avatar" image={{ altText: 'avatar', src: String(profilePicture) }} />
      </div>
      <div className="c-mobile-profile-block__bio-section">
        <h1 className="c-mobile-profile-block__user-name">{name}</h1>
        <p className="c-mobile-profile-block__bio">{bio}</p>
      </div>
    </div>
  );
};

export { MobileProfileBlock };

import { INavItems } from './Header.types';

const GRAM_DESKTOP_NAV_ITEMS: INavItems[] = [
  { additionalClassNames: 'c-desktop-header__icon mr', href: '/#', iconId: 'home_icon' },
  { additionalClassNames: 'c-desktop-header__icon mr', href: '/#', iconId: 'message_icon' },
  { additionalClassNames: 'c-desktop-header__icon mr', href: '/#', iconId: 'plus_icon' },
  { additionalClassNames: 'c-desktop-header__icon mr', href: '/#', iconId: 'place_icon' },
  { additionalClassNames: 'c-desktop-header__icon mr', href: '/#', iconId: 'like_icon' },
];

const GRAM_MOBILE_NAV_ITEMS: INavItems[] = [
  { additionalClassNames: 'c-mobile-header__icon mr', href: '/#', iconId: 'notification_icon' },
  { additionalClassNames: 'c-mobile-header__icon', href: '/#', iconId: 'settings_icon' },
];

const DESKTOP_NAV_ITEMS: INavItems[] = [];

const MOBILE_NAV_ITEMS: INavItems[] = [];

export { DESKTOP_NAV_ITEMS, MOBILE_NAV_ITEMS, GRAM_DESKTOP_NAV_ITEMS, GRAM_MOBILE_NAV_ITEMS };

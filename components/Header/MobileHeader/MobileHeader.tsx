/* eslint-disable @typescript-eslint/indent */
import cx from 'classnames';

import { MobileHeaderProps } from './MobileHeader.types';

import { Icon } from '../../Icon';

const MobileHeader = ({ additionalClassNames, avatar, navIcons, handle }: MobileHeaderProps) => {
  const mobileHeaderClassNames = cx('c-mobile-header', additionalClassNames);

  return (
    <div className={mobileHeaderClassNames}>
      <div className="c-mobile-header__wrapper">
        {avatar}
        <span className="c-mobile-header__user-name">{handle}</span>
        <div className="c-mobile-header__nav-icons">
          {navIcons
            ? navIcons.map(({ additionalClassNames: additionalIconClassNames, href, iconId }) => {
                return (
                  <a key={`nav-item-${String(iconId)}`} href={href}>
                    <Icon additionalClassNames={additionalIconClassNames} iconId={iconId} />
                  </a>
                );
              })
            : null}
        </div>
      </div>
    </div>
  );
};

export { MobileHeader };

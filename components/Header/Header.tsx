import { DesktopHeader } from './DesktopHeader';
import { HeaderProps } from './Header.types';

const Header = ({ additionalClassNames, avatar = false, navIcons, handle }: HeaderProps) => {
  return (
    <header>
      <DesktopHeader additionalClassNames={additionalClassNames} avatar={avatar} handle={handle} navIcons={navIcons} />
    </header>
  );
};

export { Header };

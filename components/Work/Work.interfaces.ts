export interface IWork {
  companyName: string;
  description: string[];
}

export interface IDBWorks {
  professionalWorks: IWork[];
}

const COMPANY_LOGOS = Object.freeze({
  'Seer Data & Analytics': 'seer-logo',
  Netfront: 'netfront-logo',
  'Are Media': 'are-logo',
  Akcelo: 'akcelo-logo',
});

export { COMPANY_LOGOS };

/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from 'react';

import { useLazyQuery } from '@apollo/client';
import cx from 'classnames';
import { useToast } from 'hooks';

import { Icon } from 'components/Icon';
import { Preloader } from 'components/Preloader';

import { GET_PROFESSIONAL_WORKS } from 'middleware/profile/getProfessionalWorks';

import { COMPANY_LOGOS } from './Work.constants';
import { IDBWorks, IWork } from './Work.interfaces';

import { client } from '../../middleware/client';

const Work = () => {
  const { handleToastError } = useToast();

  const [work, setWork] = useState<IWork[]>([]);

  const [getProfessionalWorks, { loading: isProfessionalWorksLoading }] = useLazyQuery(GET_PROFESSIONAL_WORKS, {
    client,
    onCompleted: (response: IDBWorks) => {
      const { professionalWorks } = response;

      setWork(professionalWorks);
    },
    onError: (error) => {
      handleToastError({
        error,
      });
    },
  });

  useEffect(() => {
    void getProfessionalWorks();
  }, []);

  const isLoading = isProfessionalWorksLoading;

  return (
    <>
      <Preloader isLoading={isLoading} />
      <div className="c-my-work">
        <h2 className="c-my-work-title">My work</h2>
        {work.map(({ companyName, description }) => {
          return (
            <div key={companyName}>
              <Icon
                additionalClassNames={cx(
                  'c-company-logo',
                  `c-company-logo--${companyName.split(' ').join('').toLocaleLowerCase().replace('&', '')}`,
                )}
                iconId={COMPANY_LOGOS[companyName]}
              />
              {description.map((text: string) => (
                <p key={text} className="c-company-description-text">
                  {text}
                </p>
              ))}
            </div>
          );
        })}
      </div>
    </>
  );
};

export { Work };

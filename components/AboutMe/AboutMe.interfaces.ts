import { IAdditionalClassNames, IText } from 'interfaces';

export interface AboutMeProps extends IAdditionalClassNames {
  texts?: IText[];
}

import { ButtonProps } from './Button.types';

const Button = ({ text, onClick, type = 'button' }: ButtonProps) => {
  return (
    <button type={type} onClick={onClick}>
      {text}
    </button>
  );
};

export { Button };

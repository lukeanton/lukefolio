/* eslint-disable @typescript-eslint/indent */
import cx from 'classnames';

import { COLOURS } from './Avatar.constants';
import { AvatarProps } from './Avatar.types';

const Avatar = ({ additionalClassNames, image, title }: AvatarProps) => {
  const avatarClassNames = cx('c-avatar', `c-avatar--border-${COLOURS[Math.floor(Math.random() * COLOURS.length)]}`, additionalClassNames, {
    'c-avatar__title': Boolean(title),
  });

  const avatarText =
    !image && title
      ? title
          .split(' ')
          .slice(0, 2)
          .map((item) => item.charAt(0).toUpperCase())
          .join('')
      : '';

  return (
    <>
      <span className={avatarClassNames}>
        {image ? (
          <img alt={image.altText} className="c-avatar__image" src={image.src} />
        ) : (
          <abbr className="c-avatar__initials" title={avatarText}>
            {avatarText}
          </abbr>
        )}
      </span>
    </>
  );
};

export { Avatar };

import toast from 'react-hot-toast';

import { IHandleToastErrorParams, IHandleToastSuccessParams, IUseToast } from './useToast.interfaces';

const useToast = (): IUseToast => {
  const handleToastError = ({ error }: IHandleToastErrorParams) => {
    toast.error(error.message);
  };

  const handleToastSuccess = ({ message }: IHandleToastSuccessParams) => {
    toast.success(message);
  };

  return {
    handleToastError,
    handleToastSuccess,
  };
};

export { useToast };

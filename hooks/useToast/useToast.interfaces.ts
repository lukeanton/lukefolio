export interface IHandleToastErrorParams {
  error: Error;
}

export interface IHandleToastSuccessParams {
  message: string;
}

export interface IUseToast {
  handleToastError: (params: IHandleToastErrorParams) => void;
  handleToastSuccess: (params: IHandleToastSuccessParams) => void;
}

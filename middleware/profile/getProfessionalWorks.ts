import { gql } from '@apollo/react-hooks';

export const GET_PROFESSIONAL_WORKS = gql`
  query getProfessionalWorks {
    professionalWorks {
      companyName
      description
    }
  }
`;

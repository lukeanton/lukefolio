import { gql } from '@apollo/react-hooks';

const GET_PROFILE_BY_PROFILE_ID = gql`
  query getProfile($id: ID) {
    profile(where: { id: $id }) {
      name
      handle
      bio
      profilePicture {
        url
      }
    }
  }
`;

export { GET_PROFILE_BY_PROFILE_ID };

import { gql } from '@apollo/react-hooks';

const GET_AVATAR_BY_PROFILE_ID = gql`
  query getProfile($id: ID) {
    profile(where: { id: $id }) {
      aboutMe
      handle
      profilePicture {
        url
      }
    }
  }
`;

export { GET_AVATAR_BY_PROFILE_ID };

import type { AppProps } from 'next/app';
import { Toaster } from 'react-hot-toast';

import { Icons } from '../components';

import '../styles/components.css';
import '../styles/globals.css';
import '../styles/theme.css';
import '../styles/utils.css';

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <div>
      <Icons />
      <Toaster position="top-center" reverseOrder={false} />
      <Component {...pageProps} />
    </div>
  );
}

// eslint-disable-next-line import/no-default-export
export default MyApp;

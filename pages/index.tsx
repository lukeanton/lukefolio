/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from 'react';

import { useLazyQuery } from '@apollo/client';
import { IText } from 'interfaces';
import type { NextPage } from 'next';
import { useMediaQuery } from 'react-responsive';

import { AboutMe } from 'components/AboutMe';

import { Header, Preloader, ProfileBlock, DESKTOP_NAV_ITEMS, Work } from '../components';
import { MOBILE_QUERY } from '../constants';
import { useToast } from '../hooks';
import { client } from '../middleware/client';
import { GET_AVATAR_BY_PROFILE_ID } from '../middleware/profile/getAvatarByProfileId';

const HomePage: NextPage = () => {
  const hasMatch = useMediaQuery({ query: MOBILE_QUERY });

  const { handleToastError, handleToastSuccess } = useToast();

  const [aboutMe, setAboutMe] = useState<IText[]>([]);
  const [handle, setHandle] = useState<string>('');
  const [hasMatches, setHasMatches] = useState<boolean>(false);

  const [getAvatarByProfileId, { loading: isGetAvatarByProfileId }] = useLazyQuery(GET_AVATAR_BY_PROFILE_ID, {
    client,
    onCompleted: (response) => {
      const { profile } = response;
      const { aboutMe: myAboutMe, handle: myHandle } = profile;

      setAboutMe(myAboutMe as IText[]);
      setHandle(String(myHandle));

      handleToastSuccess({
        message: 'Great! The page loaded and the world is still intact. XD',
      });
    },
    onError: (error) => {
      handleToastError({
        error,
      });
    },
  });

  useEffect(() => {
    // eslint-disable-next-line @typescript-eslint/no-floating-promises
    getAvatarByProfileId({
      variables: {
        id: process.env.MY_PROFILE_ID,
      },
    });
  }, []);

  useEffect(() => {
    setHasMatches(hasMatch);
  }, [hasMatch]);

  const isLoading = isGetAvatarByProfileId;
  const isMobile = hasMatches;

  return (
    <>
      <Preloader isLoading={isLoading} />
      <title>Home Page</title>
      <Header appLogoSrc="/images/app-logo.svg" handle={handle} isMobile={isMobile} navIcons={DESKTOP_NAV_ITEMS} />
      <div className="l-page-container">
        <ProfileBlock isMobile={isMobile} />
        <AboutMe texts={aboutMe} />
        <Work />
      </div>
    </>
  );
};

// eslint-disable-next-line import/no-default-export
export default HomePage;
